public class UsuarioBuilder {
    /*************
     * Atributos
     *************/
    private String nombre;
    private String apellido;
    private String direccion;
    private String telefono;
    private String email;

    //Constructor UsuarioBuilder();


    //Métodos / Acciones
    public UsuarioBuilder setNombre(String nombre){
        this.nombre = nombre;
        return this;
    }

    public UsuarioBuilder setApellido(String apellido){
        this.apellido = apellido;
        return this;
    }

    public UsuarioBuilder setDireccion(String direccion){
        this.direccion = direccion;
        return this;
    }

    public UsuarioBuilder setTelefono(String telefono){
        this.telefono = telefono;
        return this;
    }

    public UsuarioBuilder setEmail(String email){
        this.email = email;
        return this;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    


    public Usuario builder(){
        return new Usuario(this);
    }
    

}
