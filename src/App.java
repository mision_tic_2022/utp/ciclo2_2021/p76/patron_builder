public class App {
    public static void main(String[] args) throws Exception {

        UsuarioBuilder objBuilder = new UsuarioBuilder();

        objBuilder = objBuilder.setNombre("Juan");
        objBuilder = objBuilder.setApellido("Hernán");
        objBuilder = objBuilder.setTelefono("123456");
        objBuilder = objBuilder.setDireccion("Cra 100 #12-45");
        objBuilder = objBuilder.setEmail("juan@gmail.com");

        Usuario objUsuario_1 = objBuilder.builder();
        System.out.println(objUsuario_1);

        Usuario objUsuario_2 = new UsuarioBuilder()
        .setNombre("Andrés")
        .setApellido("Hernandez")
        .setDireccion("cra 100")
        .setTelefono("12344")
        .setEmail("andres@gmail.com")
        .builder();
        
        System.out.println(objUsuario_2);

    }
}
